CREATE OR REPLACE VIEW UNIM_PROJECT_CV_IAL AS
SELECT
       project_id                     project_id,
       name                           name,
       description                    description,
       plan_start                     plan_start,
       plan_finish                    plan_finish,
       actual_start                   actual_start,
       actual_finish                  actual_finish,
       frozen_date                    frozen_date,
       close_date                     close_date,
       cancel_date                    cancel_date,
       approved_date                  approved_date,
       manager                        manager,
       customer_id                    customer_id,
       customer_responsible           customer_responsible,
       customer_project_id            customer_project_id,
       company                        company,
       unim1app.Project_Site_API.Get_Default_Site(Project_Id) site,
       calendar_id                    calendar_id,
       program_id                     program_id,
       planned_revenue                planned_revenue,
       planned_cost                   planned_cost,
       currency_type                  currency_type,
       probability_to_win             probability_to_win,
       default_char_temp              default_char_temp,
       access_on_off                  access_on_off,
       date_created                   date_created,
       created_by                     created_by,
       date_modified                  date_modified,
       modified_by                    modified_by,
       DECODE(baseline_revision_number, 0, NULL, baseline_revision_number) baseline_revision_number,
       unim1app.Earned_Value_Method_API.Decode(earned_value_method) earned_value_method,
       earned_value_method            earned_value_method_db,
       unim1app.Material_Allocation_API.Decode(material_allocation) material_allocation,
       material_allocation            material_allocation_db,
       financially_responsible        financially_responsible,
       unim1app.Fnd_Boolean_API.Decode(budget_control_on) budget_control_on,
       budget_control_on              budget_control_on_db,
       unim1app.Fnd_Boolean_API.Decode(control_as_budgeted) control_as_budgeted,
       control_as_budgeted            control_as_budgeted_db,
       unim1app.Fnd_Boolean_API.Decode(control_on_total_budget) control_on_total_budget,
       control_on_total_budget        control_on_total_budget_db,
       proj_unique_purchase           proj_unique_purchase,
       proj_unique_sale               proj_unique_sale,
       copied_project                 copied_project,
       cost_structure_id              cost_structure_id,
       unim1app.Fnd_Boolean_API.Decode(multi_currency_budgeting) multi_currency_budgeting,
       multi_currency_budgeting       multi_currency_budgeting_db,
       project_currency_type          project_currency_type,
       budget_currency_type           budget_currency_type,
       project_currency_code          project_currency_code,
       category1_id                   category1_id,
       category2_id                   category2_id,
       unim1app.Project_Currency_Category_API.Decode(validate_remaining_against) validate_remaining_against,
       validate_remaining_against     validate_remaining_against_db,
       unim1app.Project_API.Get_Budget_Forecast(project_id) budget_forecast_id,
       selected_forecast_id           selected_forecast_id,
       unim1app.Project_API.Get_Forecast_Type_Id(selected_forecast_id) selected_forecast_type,
       unim1app.Project_API.Get_Forecast_Version_Id(selected_forecast_id) selected_forecast_version,
       unim1app.Project_Misc_Comp_Method_API.Decode(project_misc_comp_method) project_misc_comp_method,
       project_misc_comp_method       project_misc_comp_method_db,
       internal_rental_price_list     internal_rental_price_list,
       unim1app.Fnd_Boolean_API.Decode(plan_project_transaction) plan_project_transaction,
       plan_project_transaction       plan_project_transaction_db,
       work_day_to_hours_conv         work_day_to_hours_conv,
       text_id$ text_id$,
       v.rowstate                       objstate,
       unim1app.Project_API.Finite_State_Events__(rowstate)                     objevents,
       unim1app.Project_API.Finite_State_Decode__(rowstate)                         state,
       v.rowkey                         objkey,
       to_char(v.rowversion,'YYYYMMDDHH24MISS') objversion,
       v.rowid                          objid
      ,unim1app.Project_Cfp.Get_Cf$_Alternat_Course_Action(v.project_id) AS CF$_ALTERNAT_COURSE_ACTION
      ,unim1app.Project_Cfp.Get_Cf$_Alt_Courses_Action(v.project_id) AS CF$_ALT_COURSES_ACTION
      ,unim1app.Project_Cfp.Get_Cf$_Amt_In_Cap_Plan_Usd(v.project_id) AS CF$_AMT_IN_CAP_PLAN_USD
      ,unim1app.Project_Cfp.Get_Cf$_Any_Prev_Suppl_Local(v.project_id) AS CF$_ANY_PREV_SUPPL_LOCAL
      ,unim1app.Project_Cfp.Get_Cf$_Any_Prev_Suppl_Usd(v.project_id) AS CF$_ANY_PREV_SUPPL_USD
      ,t.CF$_DATE_UPDATED AS CF$_DATE_UPDATED
      ,unim1app.Project_Cfp.Get_Cf$_Distrubution_Driven(v.project_id) AS CF$_DISTRUBUTION_DRIVEN
      ,unim1app.Project_Cfp.Get_Cf$_Effect_If_Not_Made(v.project_id) AS CF$_EFFECT_IF_NOT_MADE
      ,unim1app.Project_Cfp.Get_Cf$_Effect_Not_Approved(v.project_id) AS CF$_EFFECT_NOT_APPROVED
      ,unim1app.Project_Cfp.Get_Cf$_Er_Amount_Local_Curr(v.project_id) AS CF$_ER_AMOUNT_LOCAL_CURR
      ,unim1app.Project_Cfp.Get_Cf$_Er_Amount_Usd(v.project_id) AS CF$_ER_AMOUNT_USD
      ,unim1app.Project_Cfp.Get_Cf$_Esr_Number_Class(v.project_id) AS CF$_ESR_NUMBER_CLASS
      ,unim1app.Project_Cfp.Get_Cf$_Health_And_Safety(v.project_id) AS CF$_HEALTH_AND_SAFETY
      ,unim1app.Project_Cfp.Get_Cf$_New_Total_Local_Curr(v.project_id) AS CF$_NEW_TOTAL_LOCAL_CURR
      ,unim1app.Project_Cfp.Get_Cf$_New_Total_Usd(v.project_id) AS CF$_NEW_TOTAL_USD      
      ,unim1app.Project_Cfp.Get_Cf$_Orig_Er_Amount_Local(v.project_id) AS CF$_ORIG_ER_AMOUNT_LOCAL
      ,unim1app.Project_Cfp.Get_Cf$_Orig_Er_Amount_Usd(v.project_id) AS CF$_ORIG_ER_AMOUNT_USD
      ,unim1app.Project_Cfp.Get_Cf$_Pollution_Control(v.project_id) AS CF$_POLLUTION_CONTROL
      ,unim1app.Project_Cfp.Get_Cf$_Pre_Cap_Amount_Local(v.project_id) AS CF$_PRE_CAP_AMOUNT_LOCAL
      ,unim1app.Project_Cfp.Get_Cf$_Pre_Cap_Amount_Us(v.project_id) AS CF$_PRE_CAP_AMOUNT_US
      ,unim1app.Project_Cfp.Get_Cf$_Project_Justification(v.project_id) AS CF$_PROJECT_JUSTIFICATION
      ,unim1app.Project_Cfp.Get_Cf$_Project_Scope(v.project_id) AS CF$_PROJECT_SCOPE
      ,unim1app.Project_Cfp.Get_Cf$_Project_Sponsor_Id(v.project_id) AS CF$_PROJECT_SPONSOR_ID
      ,t.CF$_PROJ_CARRYOVER_AMT AS CF$_PROJ_CARRYOVER_AMT
      ,t.CF$_PROJ_NA_COUNTRY AS CF$_PROJ_NA_COUNTRY_DB
      ,unim1app.ENUM_NORTH_AMER_COUNTRY_CFP.Decode(t.CF$_PROJ_NA_COUNTRY) AS CF$_PROJ_NA_COUNTRY
      ,unim1app.Project_Cfp.Get_Cf$_Sales_Driven(v.project_id) AS CF$_SALES_DRIVEN
      ,unim1app.Project_Cfp.Get_Cf$_Sibelco_Code(v.project_id) AS CF$_SIBELCO_CODE
      ,unim1app.Project_Cfp.Get_Cf$_Sup_Project_Justificat(v.project_id) AS CF$_SUP_PROJECT_JUSTIFICAT
      ,unim1app.Project_Cfp.Get_Cf$_This_Supplemental_Usd(v.project_id) AS CF$_THIS_SUPPLEMENTAL_USD
      ,unim1app.Project_Cfp.Get_Cf$_This_Suppl_Local(v.project_id) AS CF$_THIS_SUPPL_LOCAL
  --    ,unim1app.Project_Cfp.Get_Cf$_Unim_Change_Request(v.project_id) AS CF$_UNIM_CHANGE_REQUEST
      ,unim1app.Project_Cfp.Get_Cf$_Unim_Planned_Duration(v.project_id) AS CF$_UNIM_PLANNED_DURATION
FROM   unim1app.project_tab v, unim1app.PROJECT_CFT t
 WHERE v.rowkey = t.rowkey(+)
 WITH read only
;
