--2.4	Payment Terms File

SELECT DISTINCT ptt.pay_term_id         payment_term,
                ptt.description         payment_term_description,
                ptdt.days_to_due_date   days_to_due_date,
                ptdt.discount_specified discount,
                pdisc.disc_percent      discount_percent,
                pdisc.disc_days_cnt     days_to_discount_date
FROM   payment_term_tab ptt
--left join payment_term_details_tab
LEFT   OUTER JOIN payment_term_details_tab ptdt ON ptdt.company = ptt.company
                                            AND    ptdt.pay_term_id = ptt.pay_term_id
--left join payment_term_disc_tab
LEFT   OUTER JOIN payment_term_disc_tab pdisc ON ptdt.company = pdisc.company
                                          AND    ptdt.pay_term_id = pdisc.pay_term_id
                                          AND    ptdt.installment_number = pdisc.installment_number
WHERE  ptt.pay_term_id <> '*'

