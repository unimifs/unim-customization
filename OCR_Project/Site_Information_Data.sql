--2.1 SITE Information

SELECT cst.company     company_id,
       ct.name         company_name,
       cst.contract    site_id,
       cst.description site_description,
       cat.address_id  address_id,
       cat.address1    address1,
       cat.address2    address2,
       cat.zip_code    zip_code,
       cat.city        city,
       cat.state       state,
       cat.country     country

FROM   company_site_tab    cst,
       company_tab         ct,
       company_address_tab cat
WHERE  cst.company = ct.company
AND    cst.company = cat.company
