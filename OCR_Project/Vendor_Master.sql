--2.2
SELECT DISTINCT iiit.company company,
                sit.supplier_id vendor_number,
                sit.name vendor_name,
                siat.address_id address_id,
                siat.address1 address1,
                siat.address2 address2,
                '' address3,
                '' address4,
                siat.city city,
                siat.state state,
                sdtit.vat_no fed_tax_no,
                cmt.name contact_name,
                cmt.value contact_number,
                cmt.method_id comunication_method,
                iiit.pay_term_id payment_terms,
                iiit.group_id supplier_group,
                sit.corporate_form form_of_bussiness,
                st.supp_grp supplier_stat_group,
                '' category

FROM   supplier_info_tab sit
INNER  JOIN identity_invoice_info_tab iiit ON sit.supplier_id = iiit.identity
                                       AND    sit.party_type = iiit.party_type

INNER  JOIN supplier_info_address_tab siat ON sit.supplier_id = siat.supplier_id
                                       AND    sit.party_type = siat.party_type

LEFT   OUTER JOIN supplier_document_tax_info_tab sdtit ON siat.supplier_id = sdtit.supplier_id
                                                   AND    siat.address_id = sdtit.address_id
                                                   AND    iiit.company = sdtit.company

LEFT   OUTER JOIN comm_method_tab cmt ON sit.supplier_id = cmt.identity
                                  AND    sit.party_type = cmt.party_type

LEFT   OUTER JOIN supplier_tab st ON sit.supplier_id = st.vendor_no

WHERE  sit.party_type = 'SUPPLIER'