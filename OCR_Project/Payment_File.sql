--2.5	Payment File
SELECT DISTINCT p.pay_date           payment_date,
                p.payment_type_code  payment_method,
                ltsuq.ledger_item_id check_number,
                p.voucher_no         invoice_no,
                ppc.curr_amount      paid_amount

FROM   payment_tab p
INNER  JOIN payment_per_currency_tab ppc ON p.company = ppc.company
                                     AND    p.series_id = ppc.series_id
                                     AND    p.payment_id = ppc.payment_id

LEFT   OUTER JOIN ledger_transaction_su_qry ltsuq ON p.payment_id = ltsuq.payment_id
                                              AND    p.series_id = ltsuq.series_id
                                              AND    ppc.currency = ltsuq.currency
                                              AND    ppc.company = ltsuq.company

WHERE  p.payment_ledger_type = 'SUPPLIER_LEDGER'