--2.6	Invoice History File
SELECT DISTINCT i.company COMPANY,
                i.code_b SITE_ID,
                i.identity VENDOR_NUM,
                i.invoice_date INVOICE_DATE,
                i.series_id SERIES_ID,
                i.invoice_no INVOICE_NUMBER,
                (iit.net_curr_amount + iit.vat_curr_amount) INVOICE_AMOUNT
FROM   invoice_tab i
INNER  JOIN invoice_item_tab iit ON i.company = iit.company
                             AND    i.invoice_id = iit.invoice_id
                             AND    i.party_type = iit.party_type

WHERE  i.party_type = 'SUPPLIER'
AND    i.creator = 'MAN_SUPP_INVOICE_API'
AND    i.rowstate <> 'Cancelled'