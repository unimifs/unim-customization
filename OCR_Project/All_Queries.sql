--2.1 SITE Information ( OK )
SELECT cst.company     company_id,
       ct.name         company_name,
       cst.contract    site_id,
       cst.description site_description,
       cat.address_id  address_id,
       cat.address1    address1,
       cat.address2    address2,
       cat.zip_code    zip_code,
       cat.city        city,
       cat.state       state,
       cat.country     country
FROM   company_site_tab    cst,
       company_tab         ct,
       company_address_tab cat,
       site                site
WHERE  cst.company = ct.company
AND    cst.company = cat.company
AND    cst.contract = site.contract
AND    site.delivery_address = cat.address_id


--2.2
--1. Needs to pull the tax id 
--2. Need to have Phone, Tax and email as columns 
--3. Need to add the two missing custom feilds

SELECT DISTINCT iiit.company company,
                sit.supplier_id vendor_number,
                sit.name vendor_name,
                siat.address_id address_id,
                siat.address1 address1,
                siat.address2 address2,
                '' address3,
                '' address4,
                siat.city city,
                siat.state state,
                sdtit.vat_no fed_tax_no,
                cmt.name contact_name,
                --cmt.value contact_number,
                cmt.method_id comunication_method,
                DECODE(cmt.method_id,'PHONE',cmt.value) contact_number,
                DECODE(cmt.method_id,'E_MAIL',cmt.value) email,
                DECODE(cmt.method_id,'FAX',cmt.value)  fax_number,
                iiit.pay_term_id payment_terms,
                iiit.group_id supplier_group,
                sit.corporate_form form_of_bussiness,
                st.supp_grp supplier_stat_group,
                IDENTITY_INVOICE_INFO_CFP.Get_Cf$_Statement_Supplier(iiit.rowkey) Statement_Supplier,
                '' category,                
                SUPPLIER_CFP.Get_Cf$_Po_Supplier(st.rowkey) PO_Supplier

FROM   supplier_info_tab sit
INNER  JOIN identity_invoice_info_tab iiit ON sit.supplier_id = iiit.identity
                                       AND    sit.party_type = iiit.party_type

INNER  JOIN supplier_info_address_tab siat ON sit.supplier_id = siat.supplier_id
                                       AND    sit.party_type = siat.party_type

LEFT   OUTER JOIN supplier_document_tax_info_tab sdtit ON siat.supplier_id = sdtit.supplier_id
                                                   AND    siat.address_id = sdtit.address_id
                                                   AND    iiit.company = sdtit.company

LEFT   OUTER JOIN comm_method_tab cmt ON sit.supplier_id = cmt.identity
                                  AND    sit.party_type = cmt.party_type

LEFT   OUTER JOIN supplier_tab st ON sit.supplier_id = st.vendor_no

WHERE  sit.party_type = 'SUPPLIER'


--2.3 Po Daily file ( TBD)
1. TODO: work with Faizoon to Validate 
SELECT DISTINCT ct.company                   company_id,
                ct.name                      company_name,
                pot.contract                 site,
                pot.order_no                 po_number,
                polt.line_no                 po_line_no,
                polt.release_no              po_release_no,
                pot.rowstate                 po_state,
                polt.buy_qty_due             po_qty,
                polt.unit_meas               uom,
                polt.buy_unit_price          po_unit_cost,
                polt.currency_code           po_currency,
                pr.qty_arrived               received_aty,
                pr.qty_invoiced              invoice_qty,
                pr.receipt_no                po_receipt_no,
                pr.receipt_reference         receipt_reference,
                polt.invoicing_supplier      invoicing_supplier_num,
                sit.name                     invoicing_supplier_name,
                ppst.vendor_part_no          supplier_part_num,
                ppst.vendor_part_description supplier_part_description,
                polt.part_no                 part_number,
                polt.description             part_description,
                poct.charge_type             charge_type,
                Purchase_Part_API.Get_Stat_Grp(pot.contract, polt.part_no) purchase_group,
                dt.note_text       document_text,
                pot.buyer_code     buyer_id,
                pot.authorize_code coordinator_id
FROM   purchase_order_tab pot
INNER  JOIN site_tab st ON pot.contract = st.contract
INNER  JOIN company_tab ct ON st.company = ct.company
INNER  JOIN purchase_order_line_tab polt ON pot.contract = st.contract
                                     AND    pot.order_no = polt.order_no
LEFT   OUTER JOIN supplier_info_tab sit ON polt.invoicing_supplier = sit.supplier_id
LEFT   OUTER JOIN purchase_part_supplier_tab ppst ON polt.part_no = ppst.part_no
                                              AND    pot.contract = ppst.contract
                                              AND    polt.invoicing_supplier = ppst.vendor_no
LEFT   OUTER JOIN purchase_order_charge_tab poct ON pot.order_no = poct.order_no
                                             AND    pot.contract = poct.contract
LEFT   OUTER JOIN document_text_tab dt ON polt.note_id = dt.note_id
LEFT   OUTER JOIN purchase_receipt_tab pr ON polt.order_no = pr.order_no
                                      AND    polt.line_no = pr.line_no
                                      AND    polt.release_no = pr.release_no


--2.4	Payment Terms File ( OK )
SELECT DISTINCT ptt.pay_term_id         payment_term,
                ptt.description         payment_term_description,
                ptdt.days_to_due_date   days_to_due_date,
                ptdt.discount_specified discount,
                pdisc.disc_percent      discount_percent,
                pdisc.disc_days_cnt     days_to_discount_date
FROM   payment_term_tab ptt
--left join payment_term_details_tab
LEFT   OUTER JOIN payment_term_details_tab ptdt ON ptdt.company = ptt.company
                                            AND    ptdt.pay_term_id = ptt.pay_term_id
--left join payment_term_disc_tab
LEFT   OUTER JOIN payment_term_disc_tab pdisc ON ptdt.company = pdisc.company
                                          AND    ptdt.pay_term_id = pdisc.pay_term_id
                                          AND    ptdt.installment_number = pdisc.installment_number
WHERE  ptt.pay_term_id <> '*'




--2.5	Payment File
1. if payment_method should be decoded
AUTOPAY it should say ACH
CHKPAY it should say CHECK
ELSE it should be WIRE

2. INVOICE_NO columns is showing Vouxher no correct it to show the invoice no

3. Chekc no coulumn is displaying the invoice no now 

SELECT DISTINCT p.pay_date           payment_date,
                DECODE(p.payment_type_code,'AUTOPAY','ACH','CHKPAY','CHECK','WIRE')  payment_method,
                --ltsuq.ledger_item_id         check_number,
                ltsuq.ledger_item_id         invoice_no,
                ppc.curr_amount      paid_amount

FROM   payment_tab p
INNER  JOIN payment_per_currency_tab ppc ON p.company = ppc.company
                                     AND    p.series_id = ppc.series_id
                                     AND    p.payment_id = ppc.payment_id

LEFT   OUTER JOIN ledger_transaction_su_qry ltsuq ON p.payment_id = ltsuq.payment_id
                                              AND    p.series_id = ltsuq.series_id
                                              AND    ppc.currency = ltsuq.currency
                                              AND    ppc.company = ltsuq.company

WHERE  p.payment_ledger_type = 'SUPPLIER_LEDGER'




--2.6	Invoice History File ( OK : This is a one time file )
SELECT DISTINCT i.company COMPANY,
                i.code_b SITE_ID,
                i.identity VENDOR_NUM,
                i.invoice_date INVOICE_DATE,
                i.ledger_item_series_id SERIES_ID,
                i.ledger_item_id INVOICE_NUMBER,
                i.invoice_amount INVOICE_AMOUNT,
                i.currency CURRENCY
FROM   INVOICE_LEDGER_ITEM_SU_QRY i
WHERE  i.party_type_db = 'SUPPLIER'
AND    i.invoice_type = 'SUPPINV'
AND    i.state <> 'Cancelled'

--2.7 Approver File
SELECT distinct aart.company app_company,
                cct.code_b app_site,
                arrlt.authorizer_id app_reviewer_id,
                POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id) app_reviewer_name,
                cct.code_d app_project_id,
                DECODE(cct.code_d, NULL, NULL, Text_Field_Translation_API.Get_Text(aart.company, 'CODED', cct.code_d)) app_project_name,
                DECODE(cct.code_d, '%', NULL, DECODE(arrlt.sequence_no, 2, arrlt.authorizer_id, NULL)) app_p_approver1_id,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 2, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_p_approver1_name,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 3, arrlt.authorizer_id, NULL)) app_p_approver2_id,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 3, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_p_approver2_name,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 4, arrlt.authorizer_id, NULL)) app_p_approver3_id,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 4, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_p_approver3_name,
                cct.code_c app_department_id,
                DECODE(cct.code_c, NULL, NULL, Text_Field_Translation_API.Get_Text(aart.company, 'CODEC', cct.code_c)) app_department_name,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 2, arrlt.authorizer_id, NULL)) app_d_approver1_id,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 2, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_d_approver1_name,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 3, arrlt.authorizer_id, NULL)) app_d_approver2_id,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 3, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_d_approver2_name,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 4, arrlt.authorizer_id, NULL)) app_d_approver3_id,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 4, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_d_approver3_name
FROM   auth_assign_rule_tab aart
LEFT   OUTER JOIN auth_assign_rule_codepart_tab aarc ON aart.company = aarc.company
                                                 AND    aart.assign_rule = aarc.assign_rule
INNER  JOIN codestring_comb_tab cct ON aarc.posting_combination_id = cct.posting_combination_id
INNER  JOIN auth_routing_rule_line_tab arrlt ON aart.company = arrlt.company
                                         AND    aart.assign_rule = arrlt.routing_rule_id
WHERE   arrlt.authorization_role = 'AUTHORIZER'




--2.8 Employee file ( this is not right need to create new custom Lus)
1. to define Roles : 
2. to assign the roles to users. 

SELECT DISTINCT st.company company_id,
                ct.name company_name,
                uast.contract site,
                uwfru.cf$_user_id user_id,
                ugmft.user_group user_group,
                pit.name NAME,
                Fnd_User_Property_API.Get_Value(uast.userid,
                                                'SMTP_MAIL_ADDRESS') email,
                Comm_Method_API.Get_Default_Phone('PERSON', uast.userid) phone,
                pit.title title,
                uwfru.cf$_Wf_Role1 Wf_Role1,
                uwfru.cf$_Wf_Role2 Wf_Role2,
                uwfru.cf$_Wf_Role3 Wf_Role3

  FROM user_default_tab udt
 INNER JOIN user_allowed_site_tab uast
    ON udt.userid = uast.userid
 INNER JOIN site_tab st
    ON uast.contract = st.contract
 INNER JOIN company_tab ct
    ON st.company = ct.company
 INNER JOIN company_site_tab cst
    ON uast.contract = cst.contract
   AND st.company = cst.company
 INNER JOIN UNIM_WORK_FLOW_ROLE_USER_CLV uwfru
    ON udt.userid = uwfru.cf$_user_id

  LEFT OUTER JOIN user_group_member_finance_tab ugmft
    ON udt.userid = ugmft.userid
   AND st.company = ugmft.company

  LEFT OUTER JOIN person_info_tab pit
    ON udt.userid = pit.user_id


