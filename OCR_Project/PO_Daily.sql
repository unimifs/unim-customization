--2.3 Po Daily file
SELECT DISTINCT ct.company                   company_id,
                ct.name                      company_name,
                pot.contract                 site,
                pot.order_no                 po_number,
                polt.line_no                 po_line_no,
                polt.release_no              po_release_no,
                pot.rowstate                 po_state,
                polt.buy_qty_due             po_qty,
                polt.unit_meas               uom,
                polt.buy_unit_price          po_unit_cost,
                polt.currency_code           po_currency,
                pr.qty_arrived               received_aty,
                pr.qty_invoiced              invoice_qty,
                pr.receipt_no                po_receipt_no,
                pr.receipt_reference         receipt_reference,
                polt.invoicing_supplier      invoicing_supplier_num,
                sit.name                     invoicing_supplier_name,
                ppst.vendor_part_no          supplier_part_num,
                ppst.vendor_part_description supplier_part_description,
                polt.part_no                 part_number,
                polt.description             part_description,
                poct.charge_type             charge_type,
                Purchase_Part_API.Get_Stat_Grp(pot.contract, polt.part_no) purchase_group,
                dt.note_text       document_text,
                pot.buyer_code     buyer_id,
                pot.authorize_code coordinator_id
FROM   purchase_order_tab pot

INNER  JOIN site_tab st ON pot.contract = st.contract
INNER  JOIN company_tab ct ON st.company = ct.company

INNER  JOIN purchase_order_line_tab polt ON pot.contract = st.contract
                                     AND    pot.order_no = polt.order_no
LEFT   OUTER JOIN supplier_info_tab sit ON polt.invoicing_supplier = sit.supplier_id

LEFT   OUTER JOIN purchase_part_supplier_tab ppst ON polt.part_no = ppst.part_no
                                              AND    pot.contract = ppst.contract
                                              AND    polt.invoicing_supplier = ppst.vendor_no

LEFT   OUTER JOIN purchase_order_charge_tab poct ON pot.order_no = poct.order_no
                                             AND    pot.contract = poct.contract

LEFT   OUTER JOIN document_text_tab dt ON polt.note_id = dt.note_id

LEFT   OUTER JOIN purchase_receipt_tab pr ON polt.order_no = pr.order_no
                                      AND    polt.line_no = pr.line_no
                                      AND    polt.release_no = pr.release_no
