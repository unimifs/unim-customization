--2.8 Employee file

SELECT DISTINCT st.company company_id,
                ct.name company_name,
                uast.contract site,
                uast.userid user_id,
                ugmft.user_group user_group,
                cst.description NAME,
                Fnd_User_Property_API.Get_Value(uast.userid, 'SMTP_MAIL_ADDRESS') email,
                Comm_Method_API.Get_Default_Phone('PERSON', uast.userid) phone,
                pit.title title
FROM   user_default_tab udt
INNER  JOIN user_allowed_site_tab uast ON udt.userid = uast.userid
INNER  JOIN site_tab st ON uast.contract = st.contract
INNER  JOIN company_tab ct ON st.company = ct.company
INNER  JOIN company_site_tab cst ON uast.contract = cst.contract
                             AND    st.company = cst.company

LEFT   OUTER JOIN user_group_member_finance_tab ugmft ON udt.userid = ugmft.userid
                                                  AND    st.company = ugmft.company

LEFT   OUTER JOIN person_info_tab pit ON udt.userid = pit.user_id
