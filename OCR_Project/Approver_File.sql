SELECT distinct aart.company app_company,
                cct.code_b app_site,
                arrlt.authorizer_id app_reviewer_id,
                POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id) app_reviewer_name,
                cct.code_d app_project_id,
                DECODE(cct.code_d, NULL, NULL, Text_Field_Translation_API.Get_Text(aart.company, 'CODED', cct.code_d)) app_project_name,
                DECODE(cct.code_d, '%', NULL, DECODE(arrlt.sequence_no, 2, arrlt.authorizer_id, NULL)) app_p_approver1_id,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 2, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_p_approver1_name,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 3, arrlt.authorizer_id, NULL)) app_p_approver2_id,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 3, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_p_approver2_name,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 4, arrlt.authorizer_id, NULL)) app_p_approver3_id,
                DECODE(cct.code_d, '%', NULL,DECODE(arrlt.sequence_no, 4, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_p_approver3_name,
                cct.code_c app_department_id,
                DECODE(cct.code_c, NULL, NULL, Text_Field_Translation_API.Get_Text(aart.company, 'CODEC', cct.code_c)) app_department_name,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 2, arrlt.authorizer_id, NULL)) app_d_approver1_id,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 2, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_d_approver1_name,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 3, arrlt.authorizer_id, NULL)) app_d_approver2_id,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 3, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_d_approver2_name,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 4, arrlt.authorizer_id, NULL)) app_d_approver3_id,
                DECODE(cct.code_c, '%', NULL, DECODE(arrlt.sequence_no, 4, POSTING_AUTHORIZER_API.Get_Name(aart.company, arrlt.authorizer_id), NULL)) app_d_approver3_name
FROM   auth_assign_rule_tab aart
LEFT   OUTER JOIN auth_assign_rule_codepart_tab aarc ON aart.company = aarc.company
                                                 AND    aart.assign_rule = aarc.assign_rule
INNER  JOIN codestring_comb_tab cct ON aarc.posting_combination_id = cct.posting_combination_id
INNER  JOIN auth_routing_rule_line_tab arrlt ON aart.company = arrlt.company
                                         AND    aart.assign_rule = arrlt.routing_rule_id
WHERE   arrlt.authorization_role = 'AUTHORIZER'
